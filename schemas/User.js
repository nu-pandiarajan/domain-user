const mongoose = require('mongoose');
const ClientError = require('../helpers/ClientError');
const Schema = mongoose.Schema;

const schema = new Schema({
  // id: Schema.ObjectId,
  name: String,
  email: String,
  password: String,
  guest: Boolean,
  status: { type: String, default: 'new' },
  createdAt: { type: Date, default: () => new Date() },
  token: String,
  lastLoginAt: Date,
  online: String,
  lastOnlineAt: Date,
  socketId: String,
});

schema.index(
  { email: 1 },
  { unique: true, partialFilterExpression: { email: { $exists: true } } }
);

function middleware(error, doc, next) {
  // TODO: look for a better way
  if (error.name === 'MongoError' && error.code === 11000) {
    console.log('Middleware userSchema:', doc.email, error.message);
    next(new ClientError('Email address already in use'));
  } else {
    next();
  }
}

schema.post('save', middleware);
schema.post('update', middleware);

const User = mongoose.model('User', schema);
module.exports = User;
