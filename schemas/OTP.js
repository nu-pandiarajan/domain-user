const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const otpSchema = new Schema({
  _id: String,
  otp: String,
  createdAt: { type: Date, default: () => new Date() },
  expiresAt: { type: Date, default: () => new Date(), expires: '1h' },
});

const OTP = mongoose.model('OTP', otpSchema);
module.exports = OTP;
