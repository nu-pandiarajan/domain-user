var express = require('express');
var cors = require('cors');
var fs = require('fs').promises;
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const jwt = require('express-jwt');
const ClientError = require('./helpers/ClientError');

require('./db');

var app = express();

// dev settings
app.enable('trust proxy');
app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// app.use(express.static('public'));

// swagger
app.get('/api-docs', async function (req, res) {
  const swaggerui = '//swagger-ui.now.sh';
  const myorigin = `${req.protocol}://${req.headers.host}`;
  const url = `${swaggerui}?url=${myorigin}/swagger`;
  res.setHeader('Location', url);
  res.sendStatus(307);
});

app.get('/swagger', async function (req, res) {
  const buf = await fs.readFile('swagger.yml');
  res.setHeader('Content-Type', 'text/yaml');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.send(buf.toString());
});

// routes
app.use('/', indexRouter);

// user service routes
{
  const openApis = [
    '/users/signup/create',
    '/users/signup/verify',
    '/users/login',
    '/users/guest',
    '/users/forgot-password/otp',
    '/users/forgot-password/update',
  ];
  const whitelistedGuestApis = ['/users/verify'];
  const secret = process.env.JWT_USER_SECRET;
  const userAuth = jwt({ secret }).unless({ path: openApis });
  const guestAuth = function (req, res, next) {
    console.log(req.user, req.path);

    if (!req.user || !req.user.guest) next();
    else if (whitelistedGuestApis.includes(req.path)) next();
    else next(new ClientError('Access denied', 403));
  };

  app.use('/users', userAuth, guestAuth, usersRouter);
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.status(404).send({ message: 'Unknown route' });
  next();
});

// error handler
app.use(function (e, req, res, next) {
  let statusCode = e.status || 500;
  let message;
  let dev = {};

  if (e.name === 'UnauthorizedError') {
    // jwt middleware // && e.inner.name === 'JsonWebTokenError'
    console.error(e);
    message = e.message || e.inner.message || 'Please login and proceed';
  } else if (e.name === 'ClientError') {
    // custom error
    statusCode = e.statusCode;
    message = e.message;
  } else {
    // unknown error
    console.error(e);
    message = 'Something went wrong';
  }

  if (process.env.NODE_ENV !== 'production' && e.name !== 'ClientError') {
    dev._errorData = { name: e.name, message: e.message };
    dev._error = e;
  }

  res.status(statusCode).send({ success: false, message, ...dev });
});

module.exports = app;
