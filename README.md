# domain user server

### start local mongo in docker

docker run --name domain-user-mongo -p 27017:27017 -d mongo

### run local server

npm start

### swagger url

http://<host>/api-docs
