const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var ms = require('ms');

const User = require('../schemas/User');
const OTP = require('../schemas/OTP');
const ClientError = require('../helpers/ClientError');
const { getRandomNumber } = require('../helpers/utils');

class Service {
  constructor(role) {
    this.role = role;
    if (role === 'USER') this.Collection = User;
  }

  async getUsers(status, type) {
    let q = {};
    if (type === 'guest') q.guest = true;
    if (type === 'member') q.guest = undefined;
    if (status) q.status = status;

    let users = await this.Collection.find(q);
    users = users.map((u) => {
      let { _id, email, guest, status, createdAt, lastLoginAt } = u;
      return { _id, email, guest, status, createdAt, lastLoginAt };
    });
    return users;
  }

  async updateUser(userIdOrEmail, { status }) {
    if (status !== 'inactive' && status !== 'active')
      throw new ClientError('Invalid data');

    let condition = userIdOrEmail.includes('@')
      ? { email: userIdOrEmail }
      : { _id: userIdOrEmail };
    let result = await this.Collection.updateOne(condition, { status });
    if (!result.n) throw new ClientError('No such user');
  }

  /* Get User Verified by Password */
  async getByPassword(email, password) {
    const user = await this.Collection.findOne({ email });
    // no such user
    if (!user) throw new ClientError('Invalid user');

    // user not (or yet to) active
    if (user.status !== 'active') throw new ClientError('User is not active');

    // password mismatch
    let result = await bcrypt.compare(password, user.password);
    if (!result) throw new ClientError('Invalid password');

    return user;
  }

  /* Get User Verified by OTP */
  async getByOTP(email, otpNum) {
    let [user, otp] = await Promise.all([
      this.Collection.findOne({ email }),
      OTP.findOne({ _id: email, expiresAt: { $gt: new Date() } }),
    ]);

    // no such user
    if (!user) throw new ClientError('Unknown user');

    // user not (or yet to) active
    if (user.status !== 'active') throw new ClientError('User is not active');

    // otp expired
    if (!otp) throw new ClientError('OTP expired');

    // otp mismatch
    if (process.env.NODE_ENV !== 'production') otp.otp = '1234';
    if (otp.otp !== otpNum) throw new ClientError('OTP is incorrect');

    // TODO: invalidate otp once verified

    return user;
  }

  async createGuestUser() {
    return await new User({ status: 'active', guest: true }).save();
  }

  /* Create Token */
  async createToken(user) {
    const secret = process.env[`JWT_${this.role}_SECRET`];
    const payload = user.guest
      ? { id: user._id, guest: true }
      : { id: user._id, email: user.email };

    const expiresIn = this.role === 'ADMIN' ? '1d' : '30d';
    const token = jwt.sign(payload, secret, { expiresIn });
    await user.updateOne({ token, lastLoginAt: new Date() });

    return token;
  }

  /* Delete Token */
  async deleteToken(userId) {
    return await User.findByIdAndUpdate(userId, { token: null });
  }

  /* Change Password */
  async changePassword(email, oldPassword, newPassword) {
    let user = await this.Collection.findOne({ email });
    let result = await bcrypt.compare(oldPassword, user.password);
    if (!result) throw new ClientError('Old password is incorrect');

    await this.updatePassword(user, newPassword);
  }

  /* Update Password */
  async updatePassword(user, password) {
    let hash = await bcrypt.hash(password, 10);
    await user.updateOne({ password: hash });
  }

  /* Generate OTP for User */
  async generateOTP(email) {
    let user = await this.Collection.findOne({ email });
    if (!user || user.status !== 'active')
      throw new ClientError('Invalid User');

    let otpNum = getRandomNumber();
    let expiresAt = new Date(Date.now() + ms('3m')); // OTP Expires 3 Min // TODO: check once
    let otp = { _id: email, otp: otpNum, expiresAt };
    let options = { upsert: true, setDefaultsOnInsert: true };
    await OTP.updateOne({ _id: email }, otp, options);

    return otpNum;
  }
}

const adminService = new Service('ADMIN');
const userService = new Service('USER');

module.exports = { Service, userService, adminService };
