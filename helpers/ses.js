const AWS = require('aws-sdk');

AWS.config.update({ region: process.env.AWS_REGION });

async function sendEmail(params) {
  if (process.env.NODE_ENV !== 'production') return;

  var sesParams = getSESParam(params);
  var data = await new AWS.SES({ apiVersion: '2010-12-01' })
    .sendEmail(sesParams)
    .promise();
  console.log(data);
}

function getSESParam({ from, to, subject, html, text }) {
  return {
    Destination: {
      ToAddresses: Array.isArray(to) ? to : [to],
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data: html,
        },
        Text: {
          Charset: 'UTF-8',
          Data: text,
        },
      },
      Subject: {
        Charset: 'UTF-8',
        Data: subject,
      },
    },
    Source: from || 'thirunavu.c@gmail.com',
  };
}

module.exports = { sendEmail };
