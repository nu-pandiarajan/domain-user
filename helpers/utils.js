const passwordPolicy = require('../password-policy.json');

/**
 * Generate OTP (random numbers)
 * @param {number} len default 4, maximum 15
 */
function getRandomNumber(len = 4) {
  return Math.random().toString().substr(2, len);
}

/**
 * Generate random characters
 * @param {number} [length] default 9
 * @param {object} [options]
 * @param {boolean} [options.alpha]
 * @param {boolean} [options.small]
 * @param {boolean} [options.caps]
 * @param {boolean} [options.num]
 * @param {string} [options.special]
 */
function generateChars(length = 9, options) {
  const s = 'abcdefghijklmnopqrstuvwxyz';
  const c = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const n = '0123456789';

  let chars = '';
  if (!options) {
    chars = c + s + n;
  } else {
    if (options.alpha || options.caps) chars += c;
    if (options.alpha || options.small) chars += s;
    if (options.num) chars += n;
    if (options.special) chars += options.special;
  }

  let result = '';
  for (let i = 0; i < length; i++) {
    result += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return result;
}

/**
 * Generate 9 digit private code (caps and nums)
 */
function generateCode() {
  return generateChars(9, { caps: true, num: true });
}

/**
 * Validate the given text against provided policy options
 * @param {string} [text]
 * @param {object} [options] validation policy options
 * @param {boolean} [options.alpha]
 * @param {boolean} [options.small]
 * @param {boolean} [options.caps]
 * @param {boolean} [options.num]
 * @param {boolean} [options.special]
 * @param {number} [options.min]
 * @param {number} [options.max]
 */
function validateChars(text, options = {}) {
  if (!(options.min || options.max)) return false;
  const a = '(?=.*[A-Za-z])';
  const c = '(?=.*[A-Z])';
  const s = '(?=.*[a-z])';
  const n = '(?=.*\\d)';
  const sp = '(?=.*[^A-Za-z0-9\\s])';

  let chars = '';
  if (options.alpha) chars += a;
  if (options.caps) chars += c;
  if (options.small) chars += s;
  if (options.num) chars += n;
  if (options.special) chars += sp;

  const min = options.min || '';
  const max = options.max || '';
  const regex = (rx = new RegExp(`^${chars}.{${min},${max}}$`));
  return regex.test(text);
}

function checkPasswordPolicy(password) {
  return validateChars(password, passwordPolicy);
}

module.exports = { getRandomNumber, generateCode, checkPasswordPolicy };
