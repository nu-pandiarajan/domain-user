var express = require('express');
var bcrypt = require('bcrypt');
var ms = require('ms');

const User = require('../schemas/User');
const OTP = require('../schemas/OTP');
const { getRandomNumber, checkPasswordPolicy } = require('../helpers/utils');
const { sendEmail } = require('../helpers/ses');
const { userService: service } = require('../service/auth');
const ClientError = require('../helpers/ClientError');

var router = express.Router();

/* GET users listing. */
router.get('/', async function (req, res) {
  // { _id: 1, email: 1, status: 1, lastLoginAt: 1 }
  let d = await User.find({});
  res.send(d);
});

/* Signup */
// TODO: rollback db insert if error at last
// TODO: trigger otp is new user and if otp expired
router.post('/signup/create', async function (req, res, next) {
  let { email, password } = req.body;

  try {
    if (!checkPasswordPolicy(password)) throw new ClientError('Weak password');

    let hash = await bcrypt.hash(password, 10);
    let otpNum = getRandomNumber();

    // create 'user'
    let user = new User({ email, password: hash });
    let { _id, status, createdAt } = await user.save();

    // create 'otp'
    let expiresAt = new Date(Date.now() + ms('3m')); // OTP Expires 3 Min
    let otp = new OTP({ _id: email, otp: otpNum, expiresAt });
    await otp.save();

    // send email
    await sendEmail({
      to: email,
      subject: 'New User Activation',
      html: `You are just a step ahead creating your account. Please enter the OTP to complete signup. <h2>${otpNum}</h2>`,
      text: `You are just a step ahead creating your account. Please enter the OTP ${otpNum} to complete signup.`,
    });

    res.status(201).send({ _id, email, status, createdAt });
  } catch (e) {
    next(e);
  }
});

/* Signup OTP */
router.post('/signup/verify', async function (req, res, next) {
  let { email, otp: otpNum } = req.body;

  let [user, otp] = await Promise.all([
    User.findOne({ email }),
    OTP.findOne({ _id: email, expiresAt: { $gt: new Date() } }),
  ]);

  if (process.env.NODE_ENV !== 'production' && otp) otp.otp = '1234';

  if (!user) {
    res.status(400).send({ success: false, message: 'User doesnot exists' });
  } else if (user.status !== 'new') {
    res.status(400).send({ success: false, message: 'User already activated' });
  } else if (!otp) {
    res.status(400).send({ success: false, message: 'OTP expired' });
  } else if (otp.otp !== otpNum) {
    res.status(400).send({ success: false, message: 'OTP is incorrect' });
  } else {
    await user.updateOne({ status: 'active' });
    res.send({ success: true, message: 'User acccount verified' });
  }
});

/* Login */
router.post('/login', async function (req, res, next) {
  let { email, password } = req.body;

  try {
    const user = await service.getByPassword(email, password);
    const token = await service.createToken(user);
    res.send({ success: true, message: 'Login success', token });
  } catch (e) {
    next(e);
  }
});

/* Guest Login */
router.post('/guest', async function (req, res, next) {
  try {
    const user = await service.createGuestUser();
    const token = await service.createToken(user);
    res.send({ success: true, message: 'Guest Login success', token });
  } catch (e) {
    next(e);
  }
});

/* Logout */
router.post('/logout', async function (req, res, next) {
  try {
    const { id } = req.user;
    await service.deleteToken(id);
    res.send({ success: true, message: 'Logout success' });
  } catch (e) {
    next(e);
  }
});

/* Change Password */
router.post('/change-password', async function (req, res, next) {
  try {
    let { email } = req.user;
    let { oldPassword, newPassword } = req.body;

    if (!checkPasswordPolicy(newPassword))
      throw new ClientError('Weak password');

    await service.changePassword(email, oldPassword, newPassword);
    res.send({ success: true, message: 'Password changed' });
  } catch (e) {
    next(e);
  }
});

/* Forgot Password - Step 1: Send OTP */
router.post('/forgot-password/otp', async function (req, res, next) {
  try {
    let { email } = req.body;
    let otpNum = await service.generateOTP(email);

    await sendEmail({
      to: email,
      subject: 'Reset Password',
      html: `You have requested to reset your account password. Please enter the OTP to setup new password. <h2>${otpNum}</h2>`,
      text: `You have requested to reset your account password. Please enter the OTP ${otpNum} to setup new password.`,
    });

    res.send({ success: true, message: 'OTP Sent' });
  } catch (e) {
    next(e);
  }
});

/* Forgot Password - Step 2: Update */
router.post('/forgot-password/update', async function (req, res, next) {
  try {
    let { email, otp: otpNum, password } = req.body;

    if (!checkPasswordPolicy(password)) throw new ClientError('Weak password');

    let user = await service.getByOTP(email, otpNum);
    await service.updatePassword(user, password);

    res.send({ success: true, message: 'Password changed' });
  } catch (e) {
    next(e);
  }
});

// TODO: verify token is hapenning in middleware; update swagger
router.post('/verify', async function (req, res) {
  let { socketId } = req.body;
  let { id } = req.user;
  let token = req.headers.authorization.split(' ')[1];
  let user = await User.findById(id, 'token');

  if (!user || user.token !== token) {
    res.status(400).send({ success: false, message: 'Invalid Token or User' });
  } else {
    user.socketId = socketId;
    user.lastLoginAt = new Date();
    await user.save();

    res.send({ success: true, message: 'Token verified', userId: user._id });
  }
});

module.exports = router;
